# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.10.0"></a>
# [0.10.0](https://gitlab.com/td7x/s6/compare/v0.9.2...v0.10.0) (2017-07-12)


### Bug Fixes

* **ci:** chai passes no-unused-expression ([6bc7197](https://gitlab.com/td7x/s6/commit/6bc7197))
* **ci:** corrects fmt and build ([f446802](https://gitlab.com/td7x/s6/commit/f446802))
* **ci:** corrects fmt and build ([3b8ccd6](https://gitlab.com/td7x/s6/commit/3b8ccd6))
* **ci:** does not fail on prune with expired cache ([6f54634](https://gitlab.com/td7x/s6/commit/6f54634))
* **ci:** fix gitlab page deploy ([09b9576](https://gitlab.com/td7x/s6/commit/09b9576))


### Features

* **ci:** move to prettier ([0283b9c](https://gitlab.com/td7x/s6/commit/0283b9c))
* **ci:** prettier & lint-stage ([75d2203](https://gitlab.com/td7x/s6/commit/75d2203))
* **ci:** prettier & lint-stage ([7292746](https://gitlab.com/td7x/s6/commit/7292746))



<a name="0.9.2"></a>
## [0.9.2](https://gitlab.com/td7x/s6/compare/v0.9.1...v0.9.2) (2017-06-20)


### Bug Fixes

* **ci:** realative path for portability ([bbba3aa](https://gitlab.com/td7x/s6/commit/bbba3aa))
* **doc:** correct typo in code sample ([0f34738](https://gitlab.com/td7x/s6/commit/0f34738))



<a name="0.9.1"></a>
## [0.9.1](https://gitlab.com/td7x/s6/compare/v0.9.0...v0.9.1) (2017-06-19)



<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/td7x/s6/compare/v0.8.6...v0.9.0) (2017-06-19)


### Bug Fixes

* **ci:** troubleshoot existing public/ dir ([3b6afe4](https://gitlab.com/td7x/s6/commit/3b6afe4))
* **ci:** troubleshoot existing public/ dir ([e4204d0](https://gitlab.com/td7x/s6/commit/e4204d0))


### Features

* **ci:** simplify devdeps management ([cfc9dc7](https://gitlab.com/td7x/s6/commit/cfc9dc7))



<a name="0.8.6"></a>
## [0.8.6](https://gitlab.com/td7x/s6/compare/v0.8.5...v0.8.6) (2017-06-18)



<a name="0.8.5"></a>
## [0.8.5](https://gitlab.com/td7x/s6/compare/v0.8.4...v0.8.5) (2017-06-18)


### Bug Fixes

* **ci:** regenerate changelog.md ([d185569](https://gitlab.com/td7x/s6/commit/d185569))



<a name="0.8.4"></a>
## [0.8.4](https://gitlab.com/td7x/s6/compare/v0.8.3...v0.8.4) (2017-06-18)



<a name="0.8.3"></a>
## [0.8.3](https://gitlab.com/td7x/s6/compare/v0.8.2...v0.8.3) (2017-06-16)



<a name="0.8.2"></a>
## [0.8.2](https://gitlab.com/td7x/s6/compare/v0.8.1...v0.8.2) (2017-06-16)



<a name="0.8.1"></a>
## [0.8.1](https://gitlab.com/td7x/s6/compare/v0.8.1-master.4...v0.8.1) (2017-06-16)



<a name="0.8.1-master.4"></a>
## [0.8.1-master.4](https://gitlab.com/td7x/s6/compare/v0.8.1-master.3...v0.8.1-master.4) (2017-06-16)



<a name="0.8.1-master.3"></a>
## [0.8.1-master.3](https://gitlab.com/td7x/s6/compare/v0.8.1-master.2...v0.8.1-master.3) (2017-06-16)



<a name="0.8.1-master.2"></a>
## [0.8.1-master.2](https://gitlab.com/td7x/s6/compare/v0.8.1-master.1...v0.8.1-master.2) (2017-06-16)



<a name="0.8.1-master.1"></a>
## [0.8.1-master.1](https://gitlab.com/td7x/s6/compare/v0.8.1-master.0...v0.8.1-master.1) (2017-06-16)



<a name="0.8.1-master.0"></a>
## [0.8.1-master.0](https://gitlab.com/td7x/s6/compare/v0.8.0...v0.8.1-master.0) (2017-06-16)



<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/td7x/s6/compare/v0.7.1...v0.8.0) (2017-06-13)


### Features

* **ci:** lint ci spec file ([998e422](https://gitlab.com/td7x/s6/commit/998e422))



<a name="0.7.1"></a>
## [0.7.1](https://gitlab.com/td7x/s6/compare/v0.7.0...v0.7.1) (2017-06-13)



<a name="0.7.0"></a>
# [0.7.0](https://gitlab.com/td7x/s6/compare/v0.6.1...v0.7.0) (2017-06-13)


### Bug Fixes

* **ci:** missing api doc artifacts ([a3f342c](https://gitlab.com/td7x/s6/commit/a3f342c))
* **ci:** missing api doc artifacts path ([b76640f](https://gitlab.com/td7x/s6/commit/b76640f))


### Features

* **ci:** initial cicd spec ([a05ec40](https://gitlab.com/td7x/s6/commit/a05ec40))
* **ci:** prepush is deprecated in npm[@5](https://github.com/5) ([8867e6e](https://gitlab.com/td7x/s6/commit/8867e6e))



<a name="0.6.1"></a>
## [0.6.1](https://gitlab.com/td7x/s6/compare/v0.6.0...v0.6.1) (2017-06-12)



<a name="0.6.0"></a>
# [0.6.0](https://gitlab.com/td7x/s6/compare/v0.5.1...v0.6.0) (2017-06-12)



<a name="0.5.1"></a>
## [0.5.1](https://gitlab.com/td7x/s6/compare/v0.5.0...v0.5.1) (2017-06-12)


### Bug Fixes

* **core:** removes unnecessary depends ([8e8855f](https://gitlab.com/td7x/s6/commit/8e8855f))


### Features

* **core:** throw custom error message when failing to get ([47606ba](https://gitlab.com/td7x/s6/commit/47606ba))



<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/td7x/s6/compare/v0.4.1...v0.5.0) (2017-06-08)


### Features

* **ci:** increased coverage ([54495ed](https://gitlab.com/td7x/s6/commit/54495ed))



<a name="0.4.1"></a>
## [0.4.1](https://gitlab.com/td7x/s6/compare/v0.4.0...v0.4.1) (2017-06-08)



<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/td7x/s6/compare/v0.4.0-0...v0.4.0) (2017-06-08)


### Features

* **core:** put puts multiple objects ([d4b4bcb](https://gitlab.com/td7x/s6/commit/d4b4bcb))
* **core:** put() places multiple env to S3 ([57a5c24](https://gitlab.com/td7x/s6/commit/57a5c24))



<a name="0.4.0-0"></a>
# [0.4.0-0](https://gitlab.com/td7x/s6/compare/0.4.0-0...v0.4.0-0) (2017-06-06)


### Features

* **cli:** initial cli exe script ([631f42f](https://gitlab.com/td7x/s6/commit/631f42f))



<a name="0.3.2"></a>
## [0.3.2](https://gitlab.com/td7x/s6/compare/0.3.2...v0.3.2) (2017-06-06)



<a name="0.3.1"></a>
## [0.3.1](https://gitlab.com/td7x/s6/compare/0.3.1...v0.3.1) (2017-06-06)



<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/td7x/s6/compare/0.3.0...v0.3.0) (2017-06-05)


### Features

* **ci:** improve typedoc config ([cdec7a3](https://gitlab.com/td7x/s6/commit/cdec7a3))
* **config:** add getSync() & remove AWS SDK deps ([697170e](https://gitlab.com/td7x/s6/commit/697170e))
* **config:** incorperate generic S3Client type ([b79b0c2](https://gitlab.com/td7x/s6/commit/b79b0c2))
* **core:** default class and types exports ([ccf665c](https://gitlab.com/td7x/s6/commit/ccf665c))
* **core:** remove AWS SDK deps and moves S6 class to index ([f7d75d2](https://gitlab.com/td7x/s6/commit/f7d75d2))
* **core:** simple exports + destructured import ([327ab83](https://gitlab.com/td7x/s6/commit/327ab83))



<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/td7x/s6/compare/0.2.0...v0.2.0) (2017-05-25)


### Features

* **ci:** scope package and rep ([d2c903c](https://gitlab.com/td7x/s6/commit/d2c903c))



<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/td7x/s6/compare/0.1.0...v0.1.0) (2017-05-25)


### Bug Fixes

* **ci:** remove unused file ([01a2c8c](https://gitlab.com/td7x/s6/commit/01a2c8c))
* **code:** style to pass lint ([772b62d](https://gitlab.com/td7x/s6/commit/772b62d))


### Features

* **ci:** module specs for mocha ([2b8ef66](https://gitlab.com/td7x/s6/commit/2b8ef66))
* **ci:** update ci scripts ([ddbc466](https://gitlab.com/td7x/s6/commit/ddbc466))
* **config:** config module with basic unit tests ([c3a5485](https://gitlab.com/td7x/s6/commit/c3a5485))
* **core:** s6 module and index agg ([d0e9fea](https://gitlab.com/td7x/s6/commit/d0e9fea))
