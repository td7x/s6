# Super Simple Secrets * Simple Secure Storage

> _[td7x.gitlab.io/s6](https://td7x.gitlab.io/s6) - An NPM library and tool to sprawl secrets with S3, ease, and encryption._

S6 deploys secret files (from outside the git repo) to S3 with your `package.json` version number appended to the file name - `filename-1.2.3.ext`. The consuming app uses `S6.get()` to fetch the correct secret file from the S3 bucket for the running version. This is a simple S3 client wrapper - there is nothing special or magical... just a convenience tool. It would be super simple to implement the same in other languages following the same convention. 

## Usage

There are two separate usages of S6: 1) deploy the secrets and 2) consume the secrets.

#### install

```bash
npm install -S @td7x/s6
```

#### deploy

Rather than cli flags, S6 reads config from a file. Use `default` environment if your secrets are not environment dependent, otherwise use `NODE_ENV` values.

```json
"version": "1.2.3",
"scripts": {
  "deploy": "s6 && other-deploy-scripts.sh",
},
"s6": {
  "default": {
    "file": "../secret.json",
    "target": "s3://bucketname/app/secret.json"
  }
}
```

In this case `npm run deploy` will deploy '../secret.json' as 's3://bucketname/app/secret-1.2.3.json' with encryption and before running 'other-deploy-scripts.sh'. If S6 fails, the deploy script exist before continuing to the next script and each secret deployment is versioned just as the package.

> Versioned config (such as secrets) are an important feature of any secret management tool. Use non-versioned config tools such as AWS Parameter Store carefully.

#### consume

Consume the secret file just as any other S3.getObject() request. Again there is no magic you can use s3.getObject directly too. s6.get() will identify the correct s3 bucket and version suffixed object by reading the app's package.json.

> S6 constructor accepts an S3 client and should support any of the S3 storages but the encryption in transport and at rest is only tested on AWS S3. Checkout the API documentation -  [td7x.gitlab.io/s6](https://td7x.gitlab.io/s6).

```js
import { S6, Request, Response } from 'S6';
// or
const S6 = require('S6');

const optionalOptions = {
  File: 'package.json', // the default
  NameSpace: 's6',      // the default
  Env: 'default'        // the default if one or NODE_ENV if multiple
}

const s6 = new S6(new S3(), optionalOptions);

s6.get().then((res) => console.log(res.Data));
```

> The ReadOnly ACL is used. The app will need permission to access the bucket contents. This is automagical when using provisioners such as [Terraform](https://www.terraform.io/) or [Handel](https://github.com/byu-oit/handel/issues/180) for your app's IaC and the bucket creation.

## Contributing

Please fork and open a merge request (include yourself in the contributers object). Specifically looking for:

1. Increasing the robustness and stability of the business case
1. Config file support other than package.json
1. Google Cloud, OpenStack, and Mino S3 support
1. Polygot monorepo of modules that use the same theory of software: pip, gem, tf, etc
