import * as fs from "fs";
import * as home from "home-court";
import * as loadJSONFile from "load-json-file";
import * as path from "path";

/* tslint:disable:no-unused-expression */

export interface Request {
  Env?: string;
  File?: string;
  NameSpace?: string;
}

export function isRequest(object: any): object is Request {
  return "File" in object || "NameSpace" in object || "Env" in object;
}

export interface Response {
  Config: any;
  Env: string;
  File: string;
  NameSpace: string;
  Version: string;
}

export function isResponse(object: any): object is Response {
  return (
    "File" in object &&
    "NameSpace" in object &&
    "Env" in object &&
    "Config" in object &&
    "Version" in object
  );
}

export function get(options?: Request): Promise<Response> {
  const opts = nomalize(options);

  const fileType = path.extname(opts.File).toLowerCase().split(".").pop();

  if (fileType === "json") {
    return loadJSONFile(opts.File).then((json: any) => {
      if (!json[opts.NameSpace]) {
        throw new Error("NAMESPACE NOT FOUND");
      }

      return Promise.resolve({
        ...opts,
        Config: json[opts.NameSpace],
        Version: json.version
      });
    });
  } else {
    throw new Error("UNSUPPORTED FILETYPE");
  }
}

export function getSync(options?: Request): Response {
  const opts = nomalize(options);

  const fileType = path.extname(opts.File).toLowerCase().split(".").pop();

  if (fileType === "json") {
    const json = loadJSONFile.sync(opts.File);

    if (!json[opts.NameSpace]) {
      throw new Error("NAMESPACE NOT FOUND");
    }

    return {
      ...opts,
      Config: json[opts.NameSpace],
      Version: json.version
    };
  } else {
    throw new Error("UNSUPPORTED FILETYPE");
  }
}

function nomalize(options?: Request): Response {
  const defaults = {
    Env: process.env.node_env ? process.env.node_env : "default",
    File: "package.json",
    NameSpace: "s6"
  };

  const opts = {
    ...defaults,
    ...options
  };

  opts.File = path.resolve(opts.File, home.path, opts.File);

  if (!fs.existsSync(opts.File)) {
    throw new Error("MISSING SECRETS FILE");
  }

  return opts as Response;
}
