import * as config from "./config";
import * as s6 from "./s6";
import { glue, isS3Client, S3Client } from "./utils";

export type Request = config.Request;
export type Response = s6.Response;

export class S6 {
  private options: s6.Request;

  /**
   * @param client  S3 client must have getObject and putObject methods but AWS S3
   * legitimately the only supported client.
   * @param opts  Default options for new S6 object to use for the get and put
   * requests. Set the File, Env, or NameSpace. S6 library provides sensible config
   * for many use cases.
   */
  constructor(client: S3Client, opts?: Request) {
    if (!isS3Client(client)) {
      throw new Error("UNSUPPORTED S3 CLIENT");
    }

    this.options = glue(config.getSync(opts), client);
  }

  /**
   * S6.put() places the configured secrets to the configured buckets with the
   * appended app version and using options from the S6 constructor unless
   * provided with the optional params object.
   * @param opts  Options for the put request, set the File, Env, NameSpace.
   * @returns     The Response will include the options used and the S3 ClientResponse.
   */
  put(opts?: s6.Request): Promise<Response> {
    return opts ? s6.put(opts) : s6.put(this.options);
  }

  /**
   * S6.get() fetches the secret file from the configured bucket using the app
   * version and using options from the S6 constructor unless provided with the
   * optional params object.
   * @param opts  Options for the get request, set the File, Env, NameSpace.
   * @returns     The Response will include the options used and the S3 ClientResponse.
   */
  get(opts?: s6.Request): Promise<Response> {
    return opts ? s6.get(opts) : s6.get(this.options);
  }
}
