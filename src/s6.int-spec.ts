import * as chai from "chai";
import "mocha";
const expect = chai.expect;

import * as S3 from "aws-sdk/clients/s3";
import * as shell from "shelljs";
import * as s6 from "./s6";

/* tslint:disable:no-unused-expression */

function bucketKeys(bucket: string, client: S3): Promise<any[]> {
  return client.listObjectsV2({ Bucket: bucket }).promise().then((res: any) => {
    const keys: any[] = [];
    res.Contents.forEach((obj: any) => {
      keys.push(obj.Key);
    });
    return Promise.resolve(keys);
  });
}

describe("s6", function() {
  this.timeout(3000);
  const s3 = new S3();

  before(() => {
    shell.exec("aws s3 rm s3://test-s6 --recursive", { silent: true });
  });

  after(() => {
    shell.exec("aws s3 rm s3://test-s6 --recursive", { silent: true });
  });

  describe("put() -> get()", () => {
    const testConfig = JSON.parse(`{
        "default": {
          "file": "./test/default.json",
          "target": "s3://test-s6/default.json"
        },
        "production": {
          "file": "./test/prod.json",
          "target": "s3://test-s6/prod.json"
        },
        "stage": {
          "file": "test/stage.json",
          "target": "s3://test-s6/stage/config.json"
        }
      }`);

    it("should place all 3 test files", async () => {
      const o = {
        Client: s3,
        Config: testConfig,
        Version: "1.2.3"
      };

      const t = [
        "default-1.2.3.json",
        "prod-1.2.3.json",
        "stage/config-1.2.3.json"
      ];

      const trial = await s6.put(o);
      const result = await bucketKeys("test-s6", o.Client);
      expect(trial).to.not.be.undefined;
      expect(result.sort()).to.deep.equal(t.sort());
    });

    it("should fetch default test file from default config", async () => {
      const o: s6.Request = {
        Client: s3,
        Config: testConfig,
        Version: "1.2.3"
      };

      const t = { password: "123*default" };

      const result = await s6.get(o);
      expect(result.Data).to.be.include(t);
    });

    it("should fetch test file from explicit request", async () => {
      const o: s6.Request = {
        Client: s3,
        Config: testConfig,
        Version: "1.2.3"
      };

      const t = { password: "123*stage" };

      const result = await s6.get({ ...o, Env: "stage" });
      expect(result.Data).to.be.include(t);
    });
  });
});
