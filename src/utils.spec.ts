import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import "mocha";
chai.use(chaiAsPromised);
const expect = chai.expect;

/* tslint:disable:no-unused-expression */

import * as utils from "./utils";

import * as S3 from "aws-sdk/clients/s3";

describe("utils", () => {
  describe("versionize()", () => {
    it("should append version to filenames with paths", () => {
      const o = "filepath/filename.json";
      const v = "1.2.3";
      expect(utils.versionize(o, v)).to.equal("filepath/filename-1.2.3.json");
    });
  });

  describe("parseS3URL()", () => {
    it("should throw error for unsupported urls", () => {
      const o = "https://www.google.com";
      expect(utils.parseS3URL.bind(utils.parseS3URL, o)).to.throw(
        "UNSUPPORTED PROTOCOL"
      );
    });

    it("should have correct Bucket and Key values", () => {
      const o = "s3://bucketname/prefix/keyname.file/";
      expect(utils.parseS3URL(o)).to.have.property(
        "Key",
        "prefix/keyname.file"
      );
      expect(utils.parseS3URL(o)).to.have.property("Bucket", "bucketname");
    });
  });

  describe("readFile()", () => {
    it("should return a promise", () => {
      const o = "package.json";
      expect(utils.readFile(o)).to.eventually.be.fulfilled;
    });
  });

  describe("Generic S3Client Type", () => {
    it("should pass isS3Client()", () => {
      const o = new S3();
      expect(utils.isS3Client(o)).to.be.true;
    });
  });
});
