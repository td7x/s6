import * as fs from "fs";
import * as url from "url";
import * as config from "./config";
import * as s6 from "./s6";

export function versionize(filename: string, version: string): string {
  return filename.replace(/(\.[^\.]+)$/, "-" + version + "$1");
}

export function parseS3URL(s: string) {
  const { protocol, pathname, hostname } = url.parse(s);

  if (!(protocol && pathname && hostname) || !protocol.includes("s3")) {
    throw new Error("UNSUPPORTED PROTOCOL");
  }

  return {
    Bucket: hostname,
    Key: pathname.replace(/^\/|\/$/g, "")
  };
}

export function readFile(path: string): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    fs.readFile(path, "utf8", (err, content) => {
      if (err) reject(err);
      else resolve(content);
    });
  });
}

export interface S3Client {
  putObject: any;
  getObject: any;
}

export function isS3Client(object: any): object is S3Client {
  return "putObject" in object && "getObject" in object;
}

export function glue(input: config.Response, client: S3Client): s6.Request {
  return {
    Client: client,
    Config: input.Config,
    Env: input.Env,
    Version: input.Version
  };
}
