import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import "mocha";
chai.use(chaiAsPromised);
const expect = chai.expect;

import * as S3 from "aws-sdk/clients/s3";
import { S6 } from "./index";

describe("class s6", () => {
  it("should treat options as optional", () => {
    const s6 = new S6(new S3());
    expect(s6).to.be.a("Object");
  });
});
