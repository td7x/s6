#!/usr/bin/env node

import * as S3 from "aws-sdk/clients/s3";
import { Response, S6 } from "./index";

const s6 = new S6(new S3());

s6
  .put()
  .then((res: Response) => {
    console.log("Success:");
    console.log(res.ClientResponses);
  })
  .catch((err: Error) => {
    console.log(err.message);
  });
