import * as utils from "./utils";

export interface Request {
  Client: utils.S3Client;
  Config: any;
  Env?: string;
  Version: string;
}

export function isRequest(object: any): object is Request {
  return "Client" in object && "Config" in object && "Version" in object;
}

export interface Response {
  ClientResponses: any[];
  Config: any;
  Data?: any;
  Env?: string;
  Version: string;
}

export function isResponse(object: any): object is Response {
  return (
    "ClientResponses" in object &&
    "Env" in object &&
    "Config" in object &&
    "Version" in object
  );
}

export function put(options: Request): Promise<Response> {
  const s3 = options.Client;
  const responses: any = [];

  for (const env in options.Config) {
    const parsed = utils.parseS3URL(options.Config[env].target);
    responses.push(
      utils.readFile(options.Config[env].file).then(body => {
        const putObjectRequest = {
          Body: body,
          Bucket: parsed.Bucket,
          Key: utils.versionize(parsed.Key, options.Version),
          ServerSideEncryption: "AES256"
        };
        return s3.putObject(putObjectRequest).promise();
      })
    );
  }

  return Promise.all(responses).then(res => {
    return Promise.resolve({ ...options, ClientResponses: res });
  });
}

export function get(options: Request): Promise<Response> {
  const s3 = options.Client;
  options.Env = options.Env ? options.Env : "default";
  const parsed = utils.parseS3URL(options.Config[options.Env].target);

  return s3
    .getObject({
      Bucket: parsed.Bucket,
      Key: utils.versionize(parsed.Key, options.Version)
    })
    .promise()
    .then((res: any) => {
      const data = JSON.parse(res.Body.toString());
      return Promise.resolve({ ...options, ClientResponses: res, Data: data });
    })
    .catch((err: Error) => {
      throw new Error("FAILED TO GET OBJECTS: " + err.message);
    });
}
