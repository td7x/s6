import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import "mocha";
chai.use(chaiAsPromised);
const expect = chai.expect;
import * as path from "path";
import * as config from "./config";

/* tslint:disable:no-unused-expression */

describe("config", () => {
  describe("type checkers", () => {
    it("should work on Request", () => {
      const t: any = {
        Env: "default",
        File: "package.json",
        NameSpace: "s6"
      };

      const f: any = {
        bar: "default",
        foo: "package.json"
      };

      expect(config.isRequest(t)).to.be.true;
      expect(config.isRequest(f)).to.be.false;
    });

    it("should work on Response", () => {
      const t: any = {
        Config: {},
        Env: "default",
        File: "package.json",
        NameSpace: "s6",
        Version: "1.2.3"
      };

      const f: any = {
        File: "package.json",
        NameSpace: "s6",
        Version: "1.2.3"
      };

      expect(config.isResponse(t)).to.be.true;
      expect(config.isResponse(f)).to.be.false;
    });
  });

  describe("get", () => {
    it("should get config from defaults", () => {
      const t = {
        Env: "default",
        File: path.resolve("../s6", "package.json"),
        NameSpace: "s6"
      };

      return expect(config.get()).to.eventually.include(t);
    });

    it("should get test config", async () => {
      const o: config.Request = {
        Env: "default",
        File: "package.json",
        NameSpace: "s6"
      };

      const t = {
        Config: {
          default: {
            file: "./test/default.json",
            target: "s3://test-s6/default.json"
          },
          production: {
            file: "./test/prod.json",
            target: "s3://test-s6/prod.json"
          },
          stage: {
            file: "test/stage.json",
            target: "s3://test-s6/stage/config.json"
          }
        },
        Env: "default",
        NameSpace: "s6"
      };

      const results = await config.get(o);
      expect(results).to.have.deep.include(t);
      expect(results.Version).to.equal(process.env.npm_package_version);
      expect(results.File).to.include("package.json");
    });

    it("should get config with partial settings", async () => {
      const o = {
        Env: "production"
      };

      const t = {
        Env: "production",
        NameSpace: "s6"
      };

      const t2 = {
        file: "./test/prod.json",
        target: "s3://test-s6/prod.json"
      };

      const results = await config.get(o);
      expect(results).to.deep.include(t);
      expect(results.Config.production).to.include(t2);
    });
  });

  describe("getSync", () => {
    it("should get test config", () => {
      const o: config.Request = {
        Env: "default",
        File: "package.json",
        NameSpace: "s6"
      };

      const t = {
        Config: {
          default: {
            file: "./test/default.json",
            target: "s3://test-s6/default.json"
          },
          production: {
            file: "./test/prod.json",
            target: "s3://test-s6/prod.json"
          },
          stage: {
            file: "test/stage.json",
            target: "s3://test-s6/stage/config.json"
          }
        },
        Env: "default",
        NameSpace: "s6"
      };

      const results = config.getSync(o);
      expect(results).to.have.deep.include(t);
      expect(results.Version).to.equal(process.env.npm_package_version);
      expect(results.File).to.include("package.json");
    });
  });
});
